// Photo.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PhotoSchema = new Schema({
    name: { 
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    },
    keyword: { 
        type: String
    },
    status: { 
        type: Boolean,
        default: true
    },
    user: { 
        type: Schema.Types.ObjectId, 
        ref: 'User'
    },
    type:{
        type: String,
        enum: ['profile', 'cover', 'photos']
    }
});

const Photo = mongoose.model('Photo', PhotoSchema);
module.exports = Photo;