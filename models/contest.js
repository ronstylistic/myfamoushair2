// Photo.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContestSchema = new Schema({
    name: { 
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    from_date: {
        type: Date,
        required: true
    },
    to_date: {
        type: Date,
        required: true
    },
    status: {
        type: Boolean,
        default: true
    }

});

const Contest = mongoose.model('Contest', ContestSchema);
module.exports = Contest;