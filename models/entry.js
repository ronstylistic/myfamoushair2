// Photo.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RateSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    star: { 
        type: Number
    }
});

const EntrySchema = new Schema({
    contest: {
        type: Schema.Types.ObjectId,
        ref: 'Contest'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    photo: { 
        type: Schema.Types.ObjectId, 
        ref: 'Photo'
    },
    gender: {
        type: String
    },
    approve: {
        type: Boolean,
        default: false
    },
    rate: [RateSchema]
    
});

const Entry = mongoose.model('Entry', EntrySchema);
module.exports = Entry;