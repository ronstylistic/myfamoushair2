// User.js
const mongoose = require('mongoose');  
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstname: { 
        type: String
    },
    middlename: { 
        type: String
    },
    lastname: { 
        type: String
    },
    company: {
        type: String
    },
    username: {
        type: String,
        unique: true
    },
    email: { 
        type: String, 
        unique: true
    },
    password: { 
        type: String,
        minlength: 8
    },
    avatar: {
        type: String
    },
    role: { 
        type: String,
        default: 'user'
    },
    account_type: {
        type: String
    },
    occupation: {
        type: String
    },
    gender: {
        type: String
    },
    verified: { 
        type: Boolean, 
        default: false
    }
});

const User = mongoose.model('User', UserSchema);
module.exports = User;