// core/config.js

/*
 * Copyright (c) 2018
 *    Author: Ronnel Bedaña - ronnelbedana2011@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

exports.buildSuccess = function (data) {
    return {
        status: 200,
        success: true,
        data: data
    };
};

exports.buildEmptySuccess = function (isSuccess) {
    return {
        status: 200,
        success: isSuccess
    };
};

exports.buildError = function (status, error) {
    return {
        status: status,
        success: false,
        error: error
    };
};