// TODO: Implement Passport

const
    passport = require('passport'),
    JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

const
    User = require('../models/user');

const
    opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
        secretOrKey: process.env.SECRET_KEY,
        issuer: process.env.ISSUER,
        jsonWebTokenOptions: {
            maxAge: process.env.TOKEN_EXPIRY
        }
    };

/**
 * PassportJS JWT Strategy
 * */
passport.use('jwt', new JwtStrategy(opts, (jwt_payload, done) => {
    User.findById(jwt_payload._id)
        .then(user => {
            if (user) {
                user.token = jwt_payload;
                done(null, user);
            } else done(null, false);
        }).catch(err => {
            console.error(err);
            done(null, false);
        });
}));

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});


module.exports = passport;