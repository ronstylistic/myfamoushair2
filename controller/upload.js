const PhotoService = require('../service/PhotoService');
const config = require('../config');
const multer = require('multer');
const uploadPhotos = multer({dest: config.DIR}).array('file', 10);

const data = {
    title: 'Upload'
}

exports.upload = (req, res, next) => {

    res.render('upload', data);
}

exports.postUpload = (req, res, next) => {

    uploadPhotos(req, res, (err) => {

        if (err) {
            //next(err);
            res.status(500).send(err);
        }

        if( req.files){
            res.status(200).send({success: true});
        }
    });
}