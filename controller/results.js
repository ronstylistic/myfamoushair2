const PhotoService = require('../service/PhotoService');

const data = {
    title: 'Results'
}

exports.load = (req, res, next) => {

    PhotoService.get({})
        .then((photos) => {
            data.photos = photos;
            res.render('results', data);
        
        }).catch(err => {
            console.error(err);
            data.error = err;
            res.render('results', data);
        }) 
    
}