const 
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcryptjs'),
    moment = require('moment');

const 
    Utils = require('../core/utils');
    AuthService = require('../service/AuthService'),
    ContestService = require('../service/ContestService');

const layout = 'admin_layout.hbs';

exports.index = (req, res, next) => {
    res.render('admin/login', {
        title: 'Admin Login',
        layout: 'login_layout.hbs'
    });
}

exports.auth = (req, res, next) => {

    let data = { title: 'Admin', layout: 'login_layout.hbs' };

    let errors = req.validationErrors();

    if(errors){
        data.validation_error = errors;
        data.form = req.body;
        res.render('admin/login', data);
    }
    else{
        const { email, password } = req.body;

        AuthService.login(email).then((user) => {

            if (user && user.role == 'admin') {

                const passwordIsValid = bcrypt.compareSync(password, user.password);

                if (!passwordIsValid) {
                    data.error = "Login Failed! Email or Password is incorrect.";
                    data.form = req.body;

                    res.render('admin/login', data);
                }
                else{
                    
                    const token = jwt.sign({
                        id: user._id,
                        issuer: process.env.ISSUER,
                        scope: ['READ', 'WRITE']
                    }, process.env.SECRET_KEY, {
                        expiresIn: process.env.TOKEN_EXPIRY
                    });
                    
                    let session = {
                        token: token,
                        profile: {
                            id: user._id,
                            email: user.email,
                            firstname: user.firstname,
                            lastname: user.lastname,
                            avatar: user.avatar
                        }
                    }

                    req.session.admin = session;
                    res.redirect('/admin/dashboard');
                }
                
            } else {
                data.error = "Sorry, email address was not found.";
                data.form = req.body;
                res.render('admin/login', data);
            }

        })
        .catch(() => {
            data.error = "Network Error, please try again!";
            res.render('admin/login', data);
        });
    }
    
};

exports.dashboard = (req, res, next) => {
    res.render('admin/dashboard', {title: 'Admin', layout: layout} );
}

exports.contest = (req, res, next) => {

    let data = {
        title: 'Contest',
        layout: layout
    }

    ContestService.get()
        .then(contest => {
            data.contests = contest.map(data => {
                return {
                    id: data._id,
                    name: data.name,
                    description: data.description,
                    from_date: moment(data.from_date).format('MMM DD, YYYY'),
                    to_date: moment(data.to_date).format('MMM DD, YYYY'),
                    status: data.status ? 'Active': 'Inactive'
                }
            });
            res.render('admin/contest/list_contest', data);
        })
        .catch(err => {
            console.error(err);
            res.render('admin/contest/list_contest', data);
        })
    
}

exports.createContest = (req, res, next) => {
    res.render('admin/contest/create_contest', {title: 'Create Contest', layout: layout} );
}

exports.editContest = (req, res, next) => {

    let data = {
        title: 'Contest',
        layout: layout
    }

    let id = req.params.id;

    ContestService.getById(id)
        .then(contestInfo => {

            data.contest = {
                _id: contestInfo._id,
                name: contestInfo.name,
                description: contestInfo.description,
                from_date: moment(contestInfo.from_date).format('YYYY-MM-DD'),
                to_date: moment(contestInfo.to_date).format('YYYY-MM-DD'),
                status: contestInfo.status
            }

            res.render('admin/contest/edit_contest', data);
            
        })
        .catch(err => {
            console.error(err);
            req.flash('error', 'There was an error occured');
            res.redirect(`/admin/contest/${id}`);
        })
   
}

exports.deleteContest = (req, res, next) => {
    let id = req.params.id;

    ContestService.delete(id)
        .then(() => {
            res.status(200).send(Utils.buildEmptySuccess(true));
        })
        .catch(err => {
            console.error(err);
            res.status(500).send({error: err});
        })
   
}

exports.postCreateContest = (req, res, next) => {

    let contestInfo = req.body;

    ContestService.create(contestInfo)
        .then(() => {
            req.flash('success', 'You have successfully created contest');
            res.redirect('/admin/contest/create');
        })
        .catch(err => {
            console.error(err);
            req.flash('error', 'There was an error occured');
            res.redirect('/admin/contest/create');
        });
}

exports.postUpdateContest = (req, res, next) => {

    let id = req.params.id;
    let contestInfo = req.body;

    ContestService.update(id, contestInfo)
        .then(() => {
            req.flash('success', 'You have successfully saved the changes');
            res.redirect(`/admin/contest/edit/${id}`);

        })
        .catch(err => {
            console.error(err);
            req.flash('error', 'There was an error occured');
            res.redirect(`/admin/contest/edit/${id}`);
        });
}

exports.users = (req, res, next) => {
    res.render('admin/users/list_users', {title: 'Users', layout: layout} );
}

exports.logout = (req, res) => {

    if (req.session.admin && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        req.session.destroy();
    }

    res.redirect('/admin');
}

