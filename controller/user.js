const UserService = require('../service/UserService');
const PhotoService = require('../service/PhotoService');

const path = require('path');
const config = require('../config');
const multer = require('multer');
const uploadPhotos = multer({ dest: config.DIR}).array('file', 10);
const changePhoto = multer({ dest: config.DIR}).single('file');


/* const changePhoto = multer({
    dest: config.DIR,
    fileFilter: function (req, file, callback) {
        let ext = path.extname(file.originalname)

        if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            return callback(new Error('Only images are allowed'))
        }

        callback(null, true)
    }
}).single('profile');
 */


const data = {
    title: 'Home',
    layout: 'user_layout.hbs',
}


/* exports.profile = (req, res, next) => {

    PhotoService.get({user: req.session.user.profile.id, type: 'photos' })
        .then((photos) => {

            data.photos = photos;
            res.render('user/profile', data);
        })
        .catch(err => {
            console.log(err);
            data.error = err;
            res.render('user/profile', data);
        });
} */

/* exports.upload = (req, res, next) => {
    res.render('user/upload', data);
} */

/* exports.postUploadPhotos = (req, res, next) => {
    
    const userId = req.session.user.profile.id;
    const photoInfo = [];

    uploadPhotos(req, res, (err) => {

        if(err){
            //next(err);
            res.status(500).send(err);
        }else{
            req.files.forEach(element => {

                let info = {
                    name: element.filename,
                    path: element.path,
                    keyword: "",
                    user: userId,
                    type: 'photos'
                }
                photoInfo.push(info);
            });

            PhotoService.uploadPhotos(photoInfo)
                .then(() => {
                    //req.flash('success_message', 'You successfully uploaded.');
                    //res.redirect('/user/upload');
                    res.status(200).send({
                        success: true
                    });
                })
                .catch((error) => {
                    //req.flash('errors', error);
                    res.status(500).send(error);
                    //res.redirect('/user/upload');
                });
        }
        
    });    
} */

/* exports.photos = (req, res) => {

    PhotoService.getMyPhotos(req.session.user.profile.id)
        .then((photos) => {
            data.photos = photos;
            res.render('user/photos', data);
        })
        .catch((error) => console.log(error));
    
} */

/* exports.changePhoto = (req, res) => {
    res.render('user/change_photo', data);
} */

exports.postChangePhoto = (req, res) => {

    changePhoto(req, res, (err) => {
        if (err) {
            //req.flash('errors', err);
            //res.redirect('/user/change_photo');
            res.status(500).send(err);
        }

        const photoInfo = {
            name: req.file.filename,
            path: req.file.path,
            keyword: "myfamoushair",
            user: req.session.user.profile.id,
            type: 'profile'   
        }

        PhotoService.create(photoInfo)
            .then((photo) => {

                let data = {
                    avatar: photo.name
                }

                UserService.update(req.session.user.profile.id, data)
                    .then((user) => {
                        res.status(200).send({success: true});

                    }).catch(error => {

                    })
               /*  req.flash('success', 'You successfully changed your Profile Photo.');
                res.redirect('/user/home'); */

                
            })
            .catch((error) => {
               /*  req.flash('errors', error);
                res.redirect('/user/change_photo'); */
                res.status(500).send(error);

            });
    });
}


/* exports.aboutMe = (req, res) => {
    res.render('user/home', data);
}
 */
/* exports.gallery = (req, res) => {
    PhotoService.get({user: req.session.user.profile.id, type: 'photos'})
        .then((photos) => {
            data.photos = photos;
            res.render('user/gallery', data);
        })
        .catch((error) => console.log(error));
} */

/* exports.updateProfile = (req, res) => {

    const id = req.session.user.profile.id;
    UserService.update(id, req.body)
        .then(() => {
            req.flash('success', 'You successfully saved changes');
            res.redirect('/user/profile');
        })
        .catch(error => {
            data.error = error;
            res.render('user/profile', data)
        });
    
} */

/* exports.logout = (req, res) => {
    
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
    }

    res.redirect('/auth/login');
} */

/* exports.createGallery = (req, res) => {
    let id = req.params.id;
    data.id = id;
    res.render('user/create_gallery', data );
} */