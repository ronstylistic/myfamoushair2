const AuthService = require('../service/AuthService');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const layout = 'login_layout.hbs';


exports.postRegister = (req, res, next) => {

    let data = { title: 'Register', layout: layout };
    let errors = req.validationErrors();

    if (errors) {
        data.validation_error = errors;
        data.form = req.body;
        res.render('register', data);
    } else {
        let hashedPassword = bcrypt.hashSync(req.body.password, 10);

        let user = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: hashedPassword
        }

        AuthService
            .register(user)
            .then(() => {
                req.flash('success', 'You have successfully created an account.  Please verify your email address by clicking the link in the email sent to you');
                res.redirect('/auth/register');
            })
            .catch(() => {
                data.error = "Sorry, Email address already in use.";
                data.form = req.body;
                res.render('register', data);
            });
    }
}


//Submit Login
exports.postLogin = (req, res, next) => {
    
    let data = { title: 'Login', layout: layout };
    let errors = req.validationErrors();

    if(errors){
        data.validation_error = errors;
        data.form = req.body;
        res.render('login', data);
    }
    else{
        const { email, password } = req.body;
        AuthService.login(email).then((user) => {

          
            if (user) {

                const passwordIsValid = bcrypt.compareSync(password, user.password);

                if (!passwordIsValid) {
                    data.error = "Login Failed! Email or Password is incorrect.";
                    data.form = req.body;
                    res.render('login', data);
                }
                else{
                    
                    const token = jwt.sign({
                        id: user._id,
                        issuer: process.env.ISSUER,
                        scope: ['READ', 'WRITE']
                    }, process.env.SECRET_KEY, {
                        expiresIn: process.env.TOKEN_EXPIRY
                    });
                    
                    let session = {
                        token: token,
                        profile: {
                            id: user._id,
                            email: user.email,
                            firstname: user.firstname,
                            lastname: user.lastname,
                            company: user.company,
                            account_type: user.account_type,
                            gender: user.gender,
                            occupation: user.occupation,
                            avatar: user.avatar
                        }
                    }

                    req.session.user = session;
                    res.redirect('/user/home');
                }
                
            } else {
                data.error = "Sorry, email address was not found.";
                data.form = req.body;
                res.render('login', data);
            }

        })
        .catch(err => {
            console.error(err);
            data.error = "Network Error, please try again!";
            res.render('login', data);
        });
    }
}

exports.forgotPassword = (req, res, next) => {
   let data = { title: 'Login', layout: layout };
    res.render('forgot', data);
}

exports.postForgotPassword = (req, res) => {

}

exports.confirmation = (req, res) => {

    let email = req.email;

    AuthService
        .verifyEmail(email)
        .then(() => {
            req.flash('success', 'Thank you for verifying your email address. Your account has been verified.');
            res.redirect('/auth/login');
        })
        .catch((error) => {
            console.log(error)
            res.render('login', { title: 'Login', layout: layout, error: "Network Error, please try again." });
        })


}