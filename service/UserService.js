const User = require('../models/user');
const PhotoService = require('../service/PhotoService');

class UserService {

    static getProfile(id){
        return Promise.all([
            User.findById(id, { password: 0}), 
            PhotoService.get({user: id, type: 'photos'}
            )])
            .catch(error => console.log('error', error));
    }

    static update(id, userInfo){
        return User.findByIdAndUpdate(id, userInfo, {new : true})
    }

    static delete(id){
        return User.findByIdAndRemove(id)
    }

    static get(query){
        return User.find(query, { password: 0 })
    }

    static create(userInfo) {    
        return User.create(userInfo)
    }
}

module.exports = UserService;