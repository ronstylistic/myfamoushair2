const User = require('../models/user');
const EmailService = require('../service/EmailService');

class AuthService {

    //register method
    static register(user) {
        return User.create(user)
        .then(user => {
            return EmailService.sendEmailVerification(user.email)
                .then(() => {
                    return user;
                })
        })
        
    }//method closing tag

    //login method
    static login(email) {
        
        return User.findOne({
            email: email
        })
        
    }//method closing tag

    static verifyEmail(email){
        return User.findOneAndUpdate({email: email}, { verified: true }, { new: true })
    }
}

module.exports = AuthService;