const 
    Entry = require('../models/entry'),
    Photo = require('../models/photo');

class EntryService {

    static create(body){
        return Entry.create(body);
    }

    static insertMany(body){
        return Entry.insertMany(body);
    }

    static getAll(query){
        return Entry.find(query)
            .populate('contest')
            .populate('user',  '-password')
            .populate('photo')
            .exec()
    }

    static findById(id){
        return Entry.findById(id)
            .populate('contest')
            .populate('user', '-password')
            .populate('photo')
            .exec()
    }

    static findByIdAndUpdate(id, body){
        return Entry.findByIdAndUpdate(id, body);
    }

    static vote(id, body){
        return Entry.findByIdAndUpdate(id,
            {
                $push: {
                    rate: {
                        user: body.user,
                        star: body.star
                    }
                }
            }, {
                safe: true,
                upsert: true
        })
    }

    static getResult(query){
        
        return Entry.aggregate([
            {
                $match: query
            },
            {
                $lookup: {
                    from: "photos",
                    localField: "photo",
                    foreignField: "_id",
                    as: "photos"
                }
            },
            {
                $unwind: '$photos'
            },
            {
                $lookup: {
                    from: "users",
                    localField: "user",
                    foreignField: "_id",
                    as: "users"
                }
            }, {
                $unwind: '$users'
            },
            {
                $project: {
                    _id: "$_id",
                    user_firstname: "$users.firstname",
                    user_lastname: "$users.lastname",
                    image: "$photos.name",
                    percentage: {
                        $ifNull: [{
                            $avg: "$rate.star"
                        }, 0]
                    }
                }
            }, {
                $sort: {
                    percentage: -1
                }
            }, {
                $limit: 5
            }
        ]);
            
    }
}

module.exports = EntryService;