const Photo = require('../models/photo');
const UserService = require('../service/UserService');

class PhotoService {

    static uploadPhotos(photoInfo) {
        return Photo.insertMany(photoInfo)
    }

    static create(photoInfo){
        return Photo.create(photoInfo)
    }
    
    static changeProfilePhoto(photoInfo) {
        return Photo.create(photoInfo)
    }

    static changeCoverPhoto(photoInfo) {
        return Photo.create(photoInfo)
            .then(photo => {
                return UserService.update(photo.user, {cover: photo.name})
                    .then(() => {
                        return { success:  true} 
                    })
        })
    }

    static get(query){
        return Photo.find(query)
    }
}

module.exports = PhotoService;