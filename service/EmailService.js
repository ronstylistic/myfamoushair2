const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'SendGrid',
    auth: {
        user: process.env.SENDGRID_USER,
        pass: process.env.SENDGRID_PASS
    }
});

class EmailService {

    static sendEmailVerification(email){

        let token = jwt.sign({
            email: email
        }, process.env.SECRET_KEY, {
            expiresIn: process.env.TOKEN_EXPIRY // expires in 24 hours
        });

        let url = `${process.env.API_URL}/auth/confirmation?confirmation_token=${token}`;
        
        let mailOptions = {
            from: '"Info" <info@myfamoushair.com>', // sender address
            to: email, // list of receivers
            subject: 'Welcome to Myfamoushair', // Subject line
            html: `<h1>Welcome to Myfamoushair</h1> <p>Please click the link below to confirm your email address: <br/> <a href="${url}">${url}</a></p>`
        };

        return transporter.sendMail(mailOptions);
    }
}

module.exports = EmailService;