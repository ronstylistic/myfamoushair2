const Contest = require('../models/contest');

class ContestService {
    
    static get(){
        return Contest.find()
    }

    static getById(id){
        return Contest.findById(id)
    }
    
    static create(contestInfo) {    
        return Contest.create(contestInfo)
    }

    static delete(id){
        return Contest.findOneAndDelete(id)
    }

    static update(id, contestInfo){
        return Contest.findByIdAndUpdate(id, contestInfo, {new : true})
    }
}

module.exports = ContestService;