const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const adminController = require('../controller/admin');
const authGuard = require('../middeware/authGuard');

const
    AuthService = require('../service/AuthService'),
    EntryService = require('../service/EntryService');
    layout = 'admin_layout.hbs';

/* GET users listing. */
router.get('/', adminController.index);
router.post('/', adminController.auth);

router.post('/create_admin', (req, res) => {

    let hashedPassword = bcrypt.hashSync(req.body.password, 10);

    let user = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: hashedPassword,
        role: 'admin',
        verified: true
    }

    AuthService
        .register(user)
        .then(() => {
            res.status(200).send({success: true});
        })
        .catch(err => {
            console.error(err);
            res.status(500).send(err);
        });
})

router.get('/logout', authGuard.isAdminLoggedIn, adminController.logout);
router.get('/dashboard', authGuard.isAdminLoggedIn, adminController.dashboard);

//contests
router.get('/contest', authGuard.isAdminLoggedIn, adminController.contest);
router.get('/contest/edit/:id', authGuard.isAdminLoggedIn, adminController.editContest);
router.delete('/contest/:id', authGuard.isAdminLoggedIn, adminController.deleteContest);
router.get('/contest/create', authGuard.isAdminLoggedIn, adminController.createContest);
router.post('/contest/create', authGuard.isAdminLoggedIn, adminController.postCreateContest);
router.post('/contest/:id', authGuard.isAdminLoggedIn, adminController.postUpdateContest);

//users
router.get('/users', authGuard.isAdminLoggedIn, adminController.users);


//entries
router.get('/entry', authGuard.isAdminLoggedIn,(req, res) => {

    EntryService.getAll({ approve: false })
        .then(result => {
            
            res.render('admin/entries/list_entries', {
                entries: result,
                title: 'Entries',
                layout: layout
            });
        })
        .catch(err => {
            console.error(err);
            return res.status(500).send(err);
        })

});

router.get('/entry/:id', authGuard.isAdminLoggedIn, (req, res)  => {

    let id = req.params.id;

    EntryService.findById(id)
        .then(entry => {
            
            res.render('admin/entries/edit_entries', {
                entry: entry,
                title: 'Entries',
                layout: layout
            });
        })
        .catch(err => {
            console.error(err);
            return res.status(500).send(err);
        })
});


router.post('/entry/:id', authGuard.isAdminLoggedIn, (req, res) => {

    let id = req.params.id;

    let body = {
        gender: req.body.gender,
        approve: req.body.approve
    }

    EntryService.findByIdAndUpdate(id, body)
        .then(entry => {

            req.flash('success', 'You successfully saved changes');
            res.redirect(`/admin/entry/${id}`);
        
        })
        .catch(err => {
            console.error(err);
            return res.status(500).send(err);
        })
});

module.exports = router;
