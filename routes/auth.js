// routes/auth.js

const 
    express = require('express'),
    router = express.Router(),
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcryptjs');


const authController = require('../controller/auth');

const 
    validator = require('../middeware/validators'),
    AuthService = require('../service/AuthService'),
    utils = require('../core/utils'),
    layout = 'login_layout.hbs';

//auth login routes
router.get('/login', (req, res) => {
    res.render('login', {title: 'Login', layout: layout});
});

router.post('/login', validator.validateCredentials, authController.postLogin);


//auth register routes
router.get('/register', (req, res) => {
    res.render('register_wizard', { title: 'Register', layout: layout });
});

//router.post('/register', validator.validateNewUser, authController.postRegister);
router.post('/register', (req, res) => {
    
    let hashedPassword = bcrypt.hashSync(req.body.password, 10);
    let user = {};

    if( req.body.accountType == 'individual'){
        user = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            username: req.body.username,
            password: hashedPassword,
            gender: req.body.gender,
            account_type: req.body.accountType,
            occupation: req.body.occupation
        }
    }
    else if(req.body.accountType == 'business'){
        user = {
            company: req.body.company,
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword,
            account_type: req.body.accountType
        }
    }
    
    AuthService.register(user)
        .then((result) => {
            res.status(200).send(utils.buildSuccess(result));
        })
        .catch(err => {
            console.error(err);
            if( err.code == 11000)
                res.status(500).send(utils.buildError(500, "Email address or username already exists."));
            else
                res.status(500).send(utils.buildError(500, "There was an error on creating an account"));
        });

    /* let errors = req.validationErrors();

    if (errors) {
        res.status(422).send({errors: errors});

    } else {
        
        
    } */
})

//forgot password
router.get('/forgot_password', authController.forgotPassword);
router.post('/forgot_password', validator.validateEmail, authController.postForgotPassword);

router.get('/confirmation', validator.verifyEmail, authController.confirmation);

module.exports = router;
