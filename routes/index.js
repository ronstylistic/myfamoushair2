const express = require('express');
const router = express.Router();

const authGuard = require('../middeware/authGuard');

const 
    authRouter = require('./auth'),
    userRouter = require('./users'),
    adminRouter = require('./admin');

const
    moment = require('moment'),
    ContestService = require('../service/ContestService'),
    EntryService = require('../service/EntryService');

/* GET home page. */
router.get('/', (req, res) => {

    let isLoggedIn = req.session.user ? true : false;

    let data = {
        title: 'Home',
        isLoggedIn: isLoggedIn,
        user: isLoggedIn ? req.session.user.profile : null,
        images: [
            {url: '/images/female1.jpeg', keyword: ''},
            {url: '/images/female2.jpeg', keyword: ''},
            {url: '/images/female3.jpeg', keyword: ''},
            {url: '/images/female4.jpeg', keyword: ''},
            {url: '/images/female5.jpeg', keyword: ''},
            {url: '/images/male1.jpeg', keyword: ''},
            {url: '/images/male2.jpeg', keyword: ''},
            {url: '/images/male3.jpeg', keyword: ''},
            {url: '/images/male4.jpeg', keyword: ''},
            {url: '/images/male5.jpeg', keyword: ''}
        ]
    }

    res.render('index', data);
});

router.get('/contest', (req, res) => {

    let isLoggedIn = req.session.user ? true : false;

    ContestService.get()
        .then(contest => {

            let mapContest = contest.map(data => {
                return {
                    id: data._id,
                    name: data.name,
                    description: data.description,
                    from_date: moment(data.from_date).format('MMM DD, YYYY'),
                    to_date: moment(data.to_date).format('MMM DD, YYYY'),
                    status: data.status
                }
            })
            
            res.render('contest', {
                title: 'Contest',
                contests: mapContest,
                user: isLoggedIn ? req.session.user.profile : null,
            })
        })
        .catch(err => {
            console.error(err);
        })
});

router.get('/contest/:id', (req, res) => {

    let id = req.params.id;
    let isLoggedIn = req.session.user ? true : false;

    ContestService.getById(id)
        .then(result => {

            let contest = {
                _id: result._id,
                name: result.name,
                description: result.description,
                from_date: moment(result.from_date).format('YYYY-MM-DD'),
                to_date: moment(result.to_date).format('YYYY-MM-DD'),
                status: result.status
            }

            res.render('single_contest', {
                title: 'Contest',
                contest: contest,
                user: isLoggedIn ? req.session.user.profile : null,
            })

        })
        .catch(err => {
            console.error(err);

        })
});

router.get('/vote', (req, res) => {

    let isLoggedIn = req.session.user ? true : false;

    EntryService.getAll({approve: true})
        .then(entries => {
            res.render('vote', {
                title: 'Vote',
                entries: entries,
                user: isLoggedIn ? req.session.user.profile : null,
            });
        })
        .catch(err => {
            console.error(err);
            return res.status(500).send(err);
        })
});

router.get('/result', async (req, res) => {

    let isLoggedIn = req.session.user ? true : false;
    
    try {
        let menWinners = await EntryService.getResult({gender: 'men'});
        let womenWinners = await EntryService.getResult({gender: 'women'});

        return res.render('results', {
            title: 'Results',
            user: isLoggedIn ? req.session.user.profile : null,
            menWinners: menWinners,
            womenWinners: womenWinners
        });

    } catch (err) {
        console.error(err);
        res.status(500).send(err);
    }
});


router.get('/about', (req, res) => {

    let isLoggedIn = req.session.user ? true : false;

    res.render('about', {
        title: 'About Us',
        user: isLoggedIn ? req.session.user.profile : null,
    })
});

router.get('/contact', (req, res) => {
    
    let isLoggedIn = req.session.user ? true : false;

    res.render('contact', {
        title: 'Contact Us',
        user: isLoggedIn ? req.session.user.profile : null,
    })
});


router.use('/auth', authRouter);
router.use('/user', authGuard.isUserLoggedIn, userRouter);
router.use('/admin', adminRouter);

module.exports = router;
