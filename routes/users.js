const express = require('express');
const router = express.Router();
const validators = require('../middeware/validators');
const multer = require('multer');
const config = require('../config');

const uploadGallery = multer({dest: config.DIR});
const uploadPhotos = multer({ dest: config.DIR}).array('file', 10);
const changePhoto = multer({ dest: config.DIR}).single('file');

const
	layout = 'user_layout.hbs',
	PhotoService = require('../service/PhotoService'),
	EntryService = require('../service/EntryService');

router.get('/', function(req, res, next) {
  res.redirect('/user/home');
});

router.get('/home', (req, res) => {

	res.render('user/home', {
		title: 'Home',
		layout: layout,
		user: req.session.user.profile
	});
});

router.get('/profile', (req, res) => {

	PhotoService.get({user: req.session.user.profile.id, type: 'photos' })
        .then((photos) => {
            res.render('user/profile', { 
				title: 'Profile', 
				layout: layout, 
				photos: photos,
				user: req.session.user.profile
			});
        })
        .catch(err => {
            console.log(err);
            res.render('user/profile', { title: 'Profile', layout: layout, error: err});
        });
});

router.post('/profile',(req, res) => {
	const id = req.session.user.profile.id;
	UserService.update(id, req.body)
		.then(() => {
			req.flash('success', 'You successfully saved changes');
			res.redirect('/user/profile');
		})
		.catch(error => {
			console.error(error);
			res.render('user/profile', {
				title: 'Profile',
				layout: layout,
				error: error
			})
		});
})

router.get('/photos', (req, res) => {
	PhotoService.getMyPhotos(req.session.user.profile.id)
		.then((photos) => {
			data.photos = photos;
			res.render('user/photos', {
				title: 'Photos',
				layout: layout,
				photos: photos
			});
		})
		.catch((error) => console.log(error));
});

router.get('/gallery', (req, res) => {
	
	PhotoService.get({user: req.session.user.profile.id, type: 'photos'})
        .then((photos) => {
            //data.photos = photos;
            res.render('user/gallery', {
				title: 'Gallery',
				layout: layout,
				photos: photos,
				user: req.session.user.profile
			});
        })
        .catch((error) => console.log(error));
});

router.get('/gallery/:id', (req, res) => {
	let id = req.params.id;
	data.id = id;

	res.render('user/create_gallery', {
		title: 'Create Gallery',
		layout: layout,
		id: id
	});
});

router.post('/gallery', uploadGallery.array('images'), (req, res) => {

	const userId = req.session.user.profile.id;
	const photoInfo = [];
	
	req.files.forEach(element => {
		let info = {
			name: element.filename,
			path: element.path,
			keyword: "",
			user: userId,
			type: 'photos'
		}
		photoInfo.push(info);
	});

	PhotoService.uploadPhotos(photoInfo)
		.then(photo => {
			
			let entryData = photo.map(data => {
				return {
					contest: req.body.id,
					user: userId,
					photo: data._id,
					approved: false,
					rate: []
				}
			})
			
			EntryService.insertMany(entryData)
				.then(() => {
					return res.status(200).send({success: true})
				})
				.catch(err => {
					console.error(err);
					return res.status(500).send({success: false})
				});
		})
		.catch(err => {
			console.error(err);
			return res.status(500).send({success: false})
		});

});

router.get('/vote/:id', (req, res) => {

	let id = req.params.id;

	EntryService.findById(id)
		.then(entry => {
			res.render('user/vote', { title: 'Vote', layout: layout, entry: entry })
		})
		.catch(err => {
			console.error(err);
			res.status(500).send(err);
		})
	
});

router.post('/vote/:id', (req, res) => {

	let userId = req.session.user.profile.id;
	let id = req.params.id;
	let body = {
		user: userId,
		star: req.body.star
	}

	EntryService.vote(id, body)
		.then(() => {
			req.flash("success", "You successfully submitted your vote");	
			res.redirect(`user/vote/${id}`);
		})
		.catch(err => {
			console.error(err);
			res.status(500).send(err);
		})
		
	
});

router.get('/upload', (req, res) => {
	res.render('user/upload', {
		title: 'Upload',
		layout: layout
	});
});


router.post('/upload', (req, res) => {
	
	const userId = req.session.user.profile.id;
	const photoInfo = [];

	uploadPhotos(req, res, (err) => {

		if (err) {
			res.status(500).send(err);
		} else {
			req.files.forEach(element => {

				let info = {
					name: element.filename,
					path: element.path,
					keyword: "",
					user: userId,
					type: 'photos'
				}
				photoInfo.push(info);
			});

			PhotoService.uploadPhotos(photoInfo)
				.then(() => {
					res.status(200).send({
						success: true
					});
				})
				.catch((error) => {
					console.error(error);
					res.status(500).send(error);
				});
		}
	});
});

router.get('/change_photo', (req, res) => {
	res.render('user/change_photo', {
		title: 'Change Photo',
		layout: layout
	});
});
router.post('/change_photo', validators.validateProfilePhoto, (req, res) => {
	
	changePhoto(req, res, (err) => {
        if (err) {
            res.status(500).send(err);
        }

        const photoInfo = {
            name: req.file.filename,
            path: req.file.path,
            keyword: "myfamoushair",
            user: req.session.user.profile.id,
            type: 'profile'   
        }

        PhotoService.create(photoInfo)
            .then((photo) => {

                let data = {
                    avatar: photo.name
                }

                UserService.update(req.session.user.profile.id, data)
                    .then((user) => {
                        res.status(200).send({success: true});

                    }).catch(error => {
						console.error(error);
						res.status(500).send(error);
                    })                
            })
            .catch((error) => {
				console.error(error);
                res.status(500).send(error);
            });
    });
});

router.get('/about_me', (req, res) => {
	res.render('user/home', {
		title: 'Home',
		layout: layout
	});
});

router.get('/logout', (req, res) => {
	if (req.session.user && req.cookies.user_sid) {
		res.clearCookie('user_sid');
	}

	res.redirect('/auth/login');
});


module.exports = router;
