Dropzone.options.submitEntry = {
    url: '/user/gallery',
    autoProcessQueue: false,
    uploadMultiple: true,
    maxFilesize: 5, // MB
    maxFiles: 10,
    method: 'post',
    paramName: function() { return 'images'; },
    parallelUploads: 10,
    init: function(){

        var myDropzone = this;

        // Update selector to match your button
        $("#submitButton").click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            myDropzone.processQueue();
        });

        myDropzone.on('errormultiple', function (file, res, xhr) {
            console.log(file.name, res);
        })
        /* myDropzone.on('sending', function (file, xhr, formData) {
            // Append all form inputs to the formData Dropzone will POST
           // var data = $('#submitEntry').serializeArray();
            //console.log(data);

            //formData.append('id', data.id);

            $.each(data, function (key, el) {
                formData.append(el.name, el.value);
            });
        }); */

        myDropzone.on('success', function (file, response) {

            if( response.success) {
                $('#notification').addClass('alert alert-success').html("You successfully submitted your entries");
            }
        });
    }
}


$(document).ready(() => {
        
    const container = document.querySelector('#itemContainer');
    const msnry = new Masonry(container, {
        itemSelector: '.resultImage'
    });
    
    $('#galleryModal').on('hidden.bs.modal', function (e) {     
        window.location = '/user/gallery';
    });

    $('#submitEntryModal').on('hidden.bs.modal', function (e) {
       //window.location = '/user/gallery';
    });

    $('#photosModal').on('hidden.bs.modal', function (e) {
        window.location = '/user/photos';
    });

    $('#changeProfileModal').on('hidden.bs.modal', function (e) {
        window.location = '/user/profile';
    });

    var table = $("#dataTable").DataTable({
         "columnDefs": [{
                 "targets": [0],
                 "visible": false,
                 "searchable": false
             }
         ]
    });

    $('#dataTable tbody').on('click', 'a.editor_remove', function (e) {
        e.preventDefault();
        let row = $(this);
        let id = $(this).attr('data-id');

        $.ajax({
            url: `/admin/contest/${id}`,
            type: 'DELETE',
            dataType: 'json',
            success: function(result) {
                
                if( result.success) {
                    table.row(row.parents('tr')).remove().draw();
                }
                
            },
            error: function(err) {
                console.error(err.responseJSON);
            }
        });
    });

    //Wizards
     //Initialize tooltips
     //$('.nav-tabs > li a[title]').tooltip();

     //Wizard
     /* $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

         var $target = $(e.target);

         if ($target.hasClass('disabled')) {
             return false;
         }
     });

     $(".next-step").click(function (e) {
         var $active = $('.wizard .nav-tabs .nav-item .active');
         var $activeli = $active.parent("li");

         $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
         $($activeli).next().find('a[data-toggle="tab"]').click();
     });


     $(".prev-step").click(function (e) {

         var $active = $('.wizard .nav-tabs .nav-item .active');
         var $activeli = $active.parent("li");

         $($activeli).prev().find('a[data-toggle="tab"]').removeClass("disabled");
         $($activeli).prev().find('a[data-toggle="tab"]').click();

     }); */

    $('input[type=radio][name=accountType]').on('change', function () {
        
        //$('#nextStep').show();

        switch($(this).val()) {
            case 'business':
                $('#businessStep2').fadeIn();
                $('#individualStep2').fadeOut();
                break;
            case 'individual':
                $('#individualStep2').fadeIn();
                $('#businessStep2').fadeOut();
                break;
        }
    })
    
    $('#individualRegForm').submit(function (e) {
        e.preventDefault();

        let formData = $('#individualRegForm').serialize();
        
        $.ajax({
            url: '/auth/register',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(result) {
                if(result.success){
                    $('#notification').addClass('alert alert-success').html("You have successfully created an account.  Please verify your email address by clicking the link in the email sent to you");
                    
                    $('#individualRegForm')[0].reset();
                    setTimeout(() => {
                        $('#notification').fadeOut();
                    }, 5000);
                    
                }  
            },
            error: function(err) {
                console.error(err.responseJSON);
                $('#notification').addClass('alert alert-danger').html(err.responseJSON.error);
            }
        });

    });

    $('#businessRegForm').submit(function (e) {
        e.preventDefault();

        let formData = $('#businessRegForm').serialize();

        $.ajax({
            url: '/auth/register',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function (result) {
                if (result.success) {
                    $('#notification').addClass('alert alert-success').html("You have successfully created an account.  Please verify your email address by clicking the link in the email sent to you");

                    $('#businessRegForm')[0].reset();
                    setTimeout(() => {
                        $('#notification').fadeOut();
                    }, 5000);

                }
            },
            error: function (err) {
                console.error(err.responseJSON);
                $('#notification').addClass('alert alert-danger').html(err.responseJSON.error);
            }
        });

    });
});

