const jwt = require('jsonwebtoken');

module.exports.validateNewUser = (req, res, next) => {

    req.checkBody('firstname', 'Firstname is required').not().isEmpty();
    req.checkBody('lastname', 'Lastname is required').not().isEmpty();
    req.checkBody('email', 'Email address is invalid').not().isEmpty().isEmail();
    req.checkBody('password', 'Password is must be minimum of 8 characters.').isLength({
        min: 8
    });

    next();
}

module.exports.validateCredentials = (req, res, next) => {
    
    req.checkBody('email', 'Email address is invalid').not().isEmpty().isEmail();
    req.checkBody('password', 'Password must no be empty').not().isEmpty();
    next();
}

module.exports.validateEmail = (req, res, next) => {
    req.checkBody('email', 'Email address is invalid').not().isEmpty().isEmail();
    next();
}

module.exports.validateProfilePhoto = (req, res, next) => {
    req.checkBody('profile', 'Please select photo to upload').not().isEmpty();
    next();
}

module.exports.validateCoverPhoto = (req, res, next) => {
    req.checkBody('cover', 'Please select photo to upload').not().isEmpty();
    next();
}

module.exports.verifyEmail = (req, res, next) => {
    
    const token = req.query.confirmation_token;

    if(!token){
        return res.status(500).send({ verify: false, message: 'No token provided.' });
    }

    jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
        if (err)
            return res.status(500).send({
                verify: false,
                message: 'Failed to verify token.'
            });

        req.email = decoded.email;
        next();
    });
}
