module.exports.isUserLoggedIn = (req, res, next) => {
    if (!req.session.user || !req.cookies.user_sid) {
        res.redirect('/auth/login');
    } 
    else {
        next();
    }
}

module.exports.isAdminLoggedIn = (req, res, next) => {
    if (!req.session.admin || !req.cookies.user_sid) {
        res.redirect('/admin');
    } else {
        next();
    }
}


